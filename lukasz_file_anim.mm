//
// HospitalTableViewCell.swift
// MaternityHospital
//
// Created by Piotr Goliński on 06.09.2018.
// Copyright © 2018 Piotr Goliński. All rights reserved.
//

import UIKit
import Cosmos

class HospitalTableViewCell: UITableViewCell {
var titleLabel = UILabel()
var subtitleLabel = UILabel()
var valueLabel = UILabel()
var rating = CosmosView()

required init?(coder aDecoder: NSCoder) {
fatalError("init(coder:) has not been implemented")
}

override func awakeFromNib() {
super.awakeFromNib()
}

override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
super.init(style: style, reuseIdentifier: reuseIdentifier)
backgroundColor = UIColor.clear
contentView.backgroundColor = UIColor.clear

let title = getMainTitle(text: "Niepubliczny Zakład Opieki Zdrowotnej Gin-Medicus Sp. z o. o. ")
title.numberOfLines = 0
self.contentView.addSubview(title)
title.mas_updateConstraints { (make) in
make?.left.equalTo()(self.contentView.mas_left)?.offset()(8)
make?.top.equalTo()(self.contentView.mas_top)?.offset()(12)
make?.right.equalTo()(self.contentView.mas_right)?.offset()(-8)
make?.height.greaterThanOrEqualTo()(15)
}

let subtitle = getSubTitile(text: "Sowińskiego 57, Wyszków")
subtitle.numberOfLines = 0
self.contentView.addSubview(subtitle)
subtitle.mas_updateConstraints { (make) in
make?.left.equalTo()(self.contentView.mas_left)?.offset()(8)
make?.top.equalTo()(title.mas_bottom)?.offset()(8)
make?.right.equalTo()(self.contentView.mas_right)?.offset()(-8)
make?.height.greaterThanOrEqualTo()(15)
}

let rat = getRating()
self.contentView.addSubview(rat)
rat.mas_updateConstraints { (make) in
make?.left.equalTo()(self.contentView.mas_left)?.offset()(8)
make?.top.equalTo()(subtitle.mas_bottom)?.offset()(8)
}

let val = getValueLabel(value: "123")
self.contentView.addSubview(val)
val.mas_updateConstraints { (make) in
make?.left.equalTo()(rat.mas_right)?.offset()(8)
make?.centerY.equalTo()(rat)
}

let layer = UIView(frame: CGRect(x: 0, y: 259, width: 375, height: 2))
layer.layer.borderWidth = 0.5
layer.layer.borderColor = UIColor(red:0.8, green:0.8, blue:0.8, alpha:1).cgColor
self.contentView.addSubview(layer)
layer.mas_updateConstraints { (make) in
make?.left.equalTo()(self.contentView.mas_left)
make?.right.equalTo()(self.contentView.mas_right)
make?.height.equalTo()(1)
make?.top.equalTo()(val.mas_bottom)?.offset()(12)
make?.bottom.equalTo()(self.contentView.mas_bottom)
}
}

public func setViews(model: Hospital){
do{
let textString = NSMutableAttributedString(string: model.name! , attributes: [
NSFontAttributeName: UIFont(name: "Roboto-Medium", size: 15)!
])
let textRange = NSRange(location: 0, length: textString.length)
let paragraphStyle = NSMutableParagraphStyle()
paragraphStyle.lineSpacing = 1.2
textString.addAttribute(NSParagraphStyleAttributeName, value:paragraphStyle, range: textRange)
textString.addAttribute(NSKernAttributeName, value: -0.36, range: textRange)
self.titleLabel.attributedText = textString
}

do{
let textString = NSMutableAttributedString(string: model.address!, attributes: [
NSFontAttributeName: UIFont(name: "Roboto-Regular", size: 15)!
])
let textRange = NSRange(location: 0, length: textString.length)
let paragraphStyle = NSMutableParagraphStyle()
paragraphStyle.lineSpacing = 1.2
textString.addAttribute(NSParagraphStyleAttributeName, value:paragraphStyle, range: textRange)
textString.addAttribute(NSKernAttributeName, value: -0.36, range: textRange)
self.subtitleLabel.attributedText = textString
}

do{
let textContent = "(\(model.rating))"
let textString = NSMutableAttributedString(string: textContent, attributes: [
NSFontAttributeName: UIFont(name: "Roboto-Regular", size: 16)!
])
let textRange = NSRange(location: 0, length: textString.length)
let paragraphStyle = NSMutableParagraphStyle()
paragraphStyle.lineSpacing = 1.19
textString.addAttribute(NSParagraphStyleAttributeName, value:paragraphStyle, range: textRange)
textString.addAttribute(NSKernAttributeName, value: -0.39, range: textRange)

self.valueLabel.attributedText = textString
}

do{
self.rating.rating = Double(model.rating)
}
}

func getMainTitle(text: String) -> UILabel{
let textLayer = self.titleLabel
textLayer.lineBreakMode = .byWordWrapping
textLayer.numberOfLines = 0
textLayer.textColor = UIColor(red:0.39, green:0.39, blue:0.39, alpha:1)
let textContent = text
let textString = NSMutableAttributedString(string: textContent, attributes: [
NSFontAttributeName: UIFont(name: "Roboto-Medium", size: 15)!
])
let textRange = NSRange(location: 0, length: textString.length)
let paragraphStyle = NSMutableParagraphStyle()
paragraphStyle.lineSpacing = 1.2
textString.addAttribute(NSParagraphStyleAttributeName, value:paragraphStyle, range: textRange)
textString.addAttribute(NSKernAttributeName, value: -0.36, range: textRange)
textLayer.attributedText = textString

return textLayer
}

func getSubTitile(text: String) ->UILabel{
let textLayer = self.subtitleLabel
textLayer.lineBreakMode = .byWordWrapping
textLayer.numberOfLines = 0
textLayer.textColor = UIColor(red:0.61, green:0.61, blue:0.61, alpha:1)
let textContent = text
let textString = NSMutableAttributedString(string: textContent, attributes: [
NSFontAttributeName: UIFont(name: "Roboto-Regular", size: 15)!
])
let textRange = NSRange(location: 0, length: textString.length)
let paragraphStyle = NSMutableParagraphStyle()
paragraphStyle.lineSpacing = 1.2
textString.addAttribute(NSParagraphStyleAttributeName, value:paragraphStyle, range: textRange)
textString.addAttribute(NSKernAttributeName, value: -0.36, range: textRange)
textLayer.attributedText = textString
return textLayer
}

func getValueLabel(value: String) -> UILabel {
let textLayer = self.valueLabel
textLayer.lineBreakMode = .byWordWrapping
textLayer.numberOfLines = 0
textLayer.textColor = UIColor(red:0.8, green:0.8, blue:0.8, alpha:1)
let textContent = "(\(value))"
let textString = NSMutableAttributedString(string: textContent, attributes: [
NSFontAttributeName: UIFont(name: "Roboto-Regular", size: 16)!
])
let textRange = NSRange(location: 0, length: textString.length)
let paragraphStyle = NSMutableParagraphStyle()
paragraphStyle.lineSpacing = 1.19
textString.addAttribute(NSParagraphStyleAttributeName, value:paragraphStyle, range: textRange)
textString.addAttribute(NSKernAttributeName, value: -0.39, range: textRange)
textLayer.attributedText = textString
return textLayer
}

func getRating() -> CosmosView{
let cosmosView = self.rating
cosmosView.settings.updateOnTouch = false
cosmosView.settings.fillMode = .half
cosmosView.settings.starSize = 25
cosmosView.settings.starMargin = 5
cosmosView.rating = 2.4
cosmosView.settings.filledImage = UIImage(named: "Yellow Big")
cosmosView.settings.emptyImage = UIImage(named: "Gray Big")

return cosmosView
}

}
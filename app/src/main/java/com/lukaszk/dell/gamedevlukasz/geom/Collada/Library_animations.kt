package com.lukaszk.dell.gamedevlukasz.geom.Collada


import com.google.gson.annotations.SerializedName

class Library_animations {

    @SerializedName("animation")
    var animation: List<Animation>? = null
}

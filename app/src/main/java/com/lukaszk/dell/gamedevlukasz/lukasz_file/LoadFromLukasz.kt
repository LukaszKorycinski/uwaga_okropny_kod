package com.lukaszk.dell.gamedevlukasz.lukasz_file

import android.content.Context
import android.content.res.Resources
import android.util.Log

import com.lukaszk.dell.gamedevlukasz.R
import com.lukaszk.dell.gamedevlukasz.geom.AnimMeshLukasz
import com.lukaszk.dell.gamedevlukasz.geom.MatrixLukasz
import com.lukaszk.dell.gamedevlukasz.geom.Vector2f
import com.lukaszk.dell.gamedevlukasz.geom.Vector3f
import com.lukaszk.dell.gamedevlukasz.geom.VertexVNT

import java.io.BufferedReader
import java.io.InputStream
import java.io.InputStreamReader
import java.util.ArrayList


class LoadFromLukasz(private val ctx: Context) {

    private val interimLukaszMesh = InterimLukaszMesh()
    private val animMeshLukasz = AnimMeshLukasz()


    private fun translateFromLukaszToOpgl() {
        for (i in interimLukaszMesh.nonIndexedGeom.indices) {

            val boneName = findBoneIndexForPositionAndNormal(interimLukaszMesh.nonIndexedGeom[i])

            if(boneName!=null) {
                animMeshLukasz.pos.add(interimLukaszMesh.nonIndexedGeom[i].v3f)
                animMeshLukasz.norm.add(interimLukaszMesh.nonIndexedGeom[i].n3f)
                animMeshLukasz.texCoord.add(interimLukaszMesh.nonIndexedGeom[i].t2f)
                animMeshLukasz.boneIndex.add(boneName)
            }
        }

        animMeshLukasz.posesQty = interimLukaszMesh.animAndIndexedGeom.posesQty
        animMeshLukasz.bonesQty = interimLukaszMesh.animAndIndexedGeom.bonesQty

        buildIndices()

        removeDuplicats()



        animMeshLukasz.bonesMatrices = interimLukaszMesh.animAndIndexedGeom.bonesMatrices


        //        for(Short s : animMeshLukasz.indices)
        //            Log.e("output", s.toString());
        //
        //        for(Vector2f s : animMeshLukasz.texCoord)
        //            Log.e("output", s.x + " "+s.y);
        //
        //        for(Vector3f s : animMeshLukasz.pos)
        //            Log.e("output", s.x + " "+s.y+" "+s.z);
        //
        //        for(Vector3f s : animMeshLukasz.norm)
        //            Log.e("output", s.x + " "+s.y+" "+s.z);

    }


    private fun buildIndices() {
        for (i in animMeshLukasz.pos.indices) {
            animMeshLukasz.indices.add(i.toShort())
        }


        for (i in animMeshLukasz.pos.indices) {
            for (j in animMeshLukasz.pos.indices) {
                if (animMeshLukasz.compare(i, j) && i != j) {
                    animMeshLukasz.indices[j] = i.toShort()
                }
            }
        }
    }


    private fun removeDuplicats() {

        var i: Int = 0
        while (i < animMeshLukasz.pos.size) {
            var deleteFlag = true
            for (j in animMeshLukasz.indices.indices) {//szukam vertexów do których nie ma odwołań w liście indexów
                if (animMeshLukasz.indices[j] == i.toShort()) {
                    deleteFlag = false
                }
            }
            if (deleteFlag) {
                animMeshLukasz.pos.removeAt(i)
                animMeshLukasz.norm.removeAt(i)
                animMeshLukasz.boneIndex.removeAt(i)
                animMeshLukasz.texCoord.removeAt(i)

                for (j in animMeshLukasz.indices.indices) {
                    if (animMeshLukasz.indices[j] > i) {
                        val tmp = animMeshLukasz.indices[j] - 1
                        animMeshLukasz.indices[j] = tmp.toShort()
                    }
                }
                i--
            }
            i++
        }

    }


    private fun findBoneIndexForPositionAndNormal(vertexVNT: VertexVNT): Int? {

        for (i in interimLukaszMesh.animAndIndexedGeom.pos.indices) {
            if (Vector3f.getDistance(vertexVNT.v3f, interimLukaszMesh.animAndIndexedGeom.pos[i]) < 0.01) {
                if (Vector3f.getDistance(vertexVNT.n3f, interimLukaszMesh.animAndIndexedGeom.norm[i]) < 0.01) {
                    return interimLukaszMesh.animAndIndexedGeom.boneIndex[i]
                }
            }
        }
        throw RuntimeException("Nie znaleziono indeksu kości (zmniejsz dokładność?)")
    }

    internal fun removeEmptyStrings(fileArray: Array<String>): Array<String> {

        val resultList = ArrayList<String>()
        for (s in fileArray) {
            if (s != "")
                resultList.add(s)
        }

        return resultList.toTypedArray()
    }

    fun load(): AnimMeshLukasz {
        try {
            val res = ctx.resources
            val in_s = res.openRawResource(R.raw.demofile2)

            var file = convertStreamToString(in_s)

            file = removeTrash(file)

            var fileArray = file.split(" ".toRegex()).dropLastWhile({ it.isEmpty() }).toTypedArray()

            fileArray = removeEmptyStrings(fileArray)


            var fileIterator = 1

            for (i in 0 until Integer.parseInt(fileArray[0])) {
                val nonIndexedGeom = VertexVNT()

                nonIndexedGeom.v3f.x = java.lang.Float.parseFloat(fileArray[fileIterator])
                fileIterator++
                nonIndexedGeom.v3f.y = java.lang.Float.parseFloat(fileArray[fileIterator])
                fileIterator++
                nonIndexedGeom.v3f.z = java.lang.Float.parseFloat(fileArray[fileIterator])
                fileIterator++

                nonIndexedGeom.n3f.x = java.lang.Float.parseFloat(fileArray[fileIterator])
                fileIterator++
                nonIndexedGeom.n3f.y = java.lang.Float.parseFloat(fileArray[fileIterator])
                fileIterator++
                nonIndexedGeom.n3f.z = java.lang.Float.parseFloat(fileArray[fileIterator])
                fileIterator++

                nonIndexedGeom.t2f.x = java.lang.Float.parseFloat(fileArray[fileIterator])
                fileIterator++
                nonIndexedGeom.t2f.y = java.lang.Float.parseFloat(fileArray[fileIterator])
                fileIterator++

                interimLukaszMesh.nonIndexedGeom.add(nonIndexedGeom)
            }


            val vertexIndicesSize = Integer.parseInt(fileArray[fileIterator])
            fileIterator++/////tu leci exception :)


            for (i in 0 until vertexIndicesSize) {
                val indexedPosition = Vector3f()

                indexedPosition.x = java.lang.Float.parseFloat(fileArray[fileIterator])
                fileIterator++
                indexedPosition.y = java.lang.Float.parseFloat(fileArray[fileIterator])
                fileIterator++
                indexedPosition.z = java.lang.Float.parseFloat(fileArray[fileIterator])
                fileIterator++

                interimLukaszMesh.animAndIndexedGeom.pos.add(indexedPosition)
            }

            for (i in 0 until vertexIndicesSize) {
                val indexedPosition = Vector3f()

                indexedPosition.x = java.lang.Float.parseFloat(fileArray[fileIterator])
                fileIterator++
                indexedPosition.y = java.lang.Float.parseFloat(fileArray[fileIterator])
                fileIterator++
                indexedPosition.z = java.lang.Float.parseFloat(fileArray[fileIterator])
                fileIterator++

                interimLukaszMesh.animAndIndexedGeom.norm.add(indexedPosition)
            }


            val bonesQty = Integer.parseInt(fileArray[fileIterator])
            fileIterator++
            val posesQty = Integer.parseInt(fileArray[fileIterator])
            fileIterator++

            interimLukaszMesh.animAndIndexedGeom.bonesQty = bonesQty
            interimLukaszMesh.animAndIndexedGeom.posesQty = posesQty

            for (i in 0 until bonesQty) {//tu jest tylko jedna macież, a powinno być tyle, ile klatek
                interimLukaszMesh.bonesNames.add(fileArray[fileIterator])
                fileIterator++

                val matrixf = ArrayList<Float>(16)

                for (j in 0 until posesQty) {
                    matrixf.add( java.lang.Float.parseFloat(fileArray[fileIterator]) )//java.lang.NumberFormatException: For input string: "gora"
                    fileIterator++
                    matrixf.add( java.lang.Float.parseFloat(fileArray[fileIterator]) )
                    fileIterator++
                    matrixf.add( java.lang.Float.parseFloat(fileArray[fileIterator]) )
                    fileIterator++
                    matrixf.add( java.lang.Float.parseFloat(fileArray[fileIterator]) )
                    fileIterator++

                    matrixf.add( java.lang.Float.parseFloat(fileArray[fileIterator]) )
                    fileIterator++
                    matrixf.add( java.lang.Float.parseFloat(fileArray[fileIterator]) )
                    fileIterator++
                    matrixf.add( java.lang.Float.parseFloat(fileArray[fileIterator]) )
                    fileIterator++
                    matrixf.add( java.lang.Float.parseFloat(fileArray[fileIterator]) )
                    fileIterator++

                    matrixf.add( java.lang.Float.parseFloat(fileArray[fileIterator]) )
                    fileIterator++
                    matrixf.add( java.lang.Float.parseFloat(fileArray[fileIterator]) )
                    fileIterator++
                    matrixf.add( java.lang.Float.parseFloat(fileArray[fileIterator]) )
                    fileIterator++
                    matrixf.add( java.lang.Float.parseFloat(fileArray[fileIterator]) )
                    fileIterator++

                    matrixf.add( java.lang.Float.parseFloat(fileArray[fileIterator]) )
                    fileIterator++
                    matrixf.add( java.lang.Float.parseFloat(fileArray[fileIterator]) )
                    fileIterator++
                    matrixf.add( java.lang.Float.parseFloat(fileArray[fileIterator]) )
                    fileIterator++
                    matrixf.add( java.lang.Float.parseFloat(fileArray[fileIterator]) )
                    fileIterator++

                    val matrix = MatrixLukasz(matrixf)

                    interimLukaszMesh.animAndIndexedGeom.bonesMatrices.add(matrix)
                }
            }


            //            for(int i=0; i<vertexIndicesSize; i++){
            //                interimLukaszMesh.animAndIndexedGeom.boneName.add("");
            //            }
            //
            //
            //            for(int i=0; i<vertexIndicesSize; i++){
            //                Integer index = Integer.parseInt(fileArray[fileIterator]); fileIterator++;
            //                interimLukaszMesh.animAndIndexedGeom.boneName.set(index, fileArray[fileIterator]); fileIterator++;
            //            }


            for (i in 0 until vertexIndicesSize) {
                interimLukaszMesh.animAndIndexedGeom.boneIndex.add(0)
            }

            for (i in 0 until vertexIndicesSize) {//tutaj werteksom przypisuje odpowiedni INDEX kości (a mam liste nameów xd)

                val vIndex = Integer.parseInt(fileArray[fileIterator])
                fileIterator++
                val boneIndex = interimLukaszMesh.bonesNames.indexOf(fileArray[fileIterator])
                fileIterator++
                interimLukaszMesh.animAndIndexedGeom.boneIndex[vIndex] = boneIndex
            }


            translateFromLukaszToOpgl()
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return animMeshLukasz
    }


    private fun removeTrash(file: String): String {
        var fileOtu = file
        fileOtu = file.replace("bone".toRegex(), "")
        fileOtu = file.replace("<Vector ".toRegex(), "")
        fileOtu = file.replace("\\(".toRegex(), "")
        fileOtu = file.replace(",".toRegex(), "")
        fileOtu = file.replace("\\)".toRegex(), "")
        fileOtu = file.replace(">".toRegex(), "")
        fileOtu = file.replace("<".toRegex(), "")
        fileOtu = file.replace("\n".toRegex(), " ")

        return fileOtu
    }


    @Throws(Exception::class)
    private fun convertStreamToString(`is`: InputStream): String {
        val reader = BufferedReader(InputStreamReader(`is`))
        val sb = StringBuilder()
        var line: String? = null

        line = reader.readLine()
        while ( line != null ) {
            sb.append(line).append("\n")
            line = reader.readLine()
        }
        reader.close()
        return sb.toString()
    }


}

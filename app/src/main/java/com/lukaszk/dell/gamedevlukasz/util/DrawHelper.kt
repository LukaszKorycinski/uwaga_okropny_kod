package com.lukaszk.dell.gamedevlukasz.util

/**
 * Created by dell on 07.06.2017.
 */

class DrawHelper {
    var program: Int = 0
    var vertexHandle: Int = 0
    var normalHandle: Int = 0
    var colorHandle: Int = 0
    var wvpHandle: Int = 0
    var witHandle: Int = 0
    var lDifColHandle: Int = 0
    var lAmbColHandle: Int = 0
    var lDirHandle: Int = 0
    internal var matVP: FloatArray? = null
}

package com.lukaszk.dell.gamedevlukasz.geom.Collada

import com.google.gson.annotations.SerializedName

import java.util.ArrayList

/**
 * Created by dell on 09.06.2017.
 */

class Content {

    @SerializedName("float_array")
    var vertex: String? = null
}

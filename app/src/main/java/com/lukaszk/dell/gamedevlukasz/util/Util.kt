package com.lukaszk.dell.gamedevlukasz.util

import android.util.Log

object Util {


    fun logLongString(tag: String, msg: String) {
        val maxLogSize = 3000
        for (i in 0..msg.length / maxLogSize) {
            val start = i * maxLogSize
            var end = (i + 1) * maxLogSize
            end = if (end > msg.length) msg.length else end
            Log.v(tag, msg.substring(start, end))
        }
    }

}

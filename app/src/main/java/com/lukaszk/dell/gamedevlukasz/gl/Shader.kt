package com.lukaszk.dell.gamedevlukasz.gl

import android.content.Context
import android.opengl.GLES20
import android.util.Log

import com.lukaszk.dell.gamedevlukasz.R
import com.lukaszk.dell.gamedevlukasz.util.FileOperation

import java.io.InputStream

import android.opengl.GLES20.GL_COMPILE_STATUS
import android.opengl.GLES20.GL_FALSE

/**
 * Created by dell on 13.06.2017.
 */

object Shader {
    var shaderProgram: Int = 0


    fun loadShader(type: Int, shaderIdRes: Int, ctx: Context): Int {
        val shaderFileInputStream = ctx.resources.openRawResource(shaderIdRes)

        var shader = GLES20.glCreateShader(type)

        val shaderString = FileOperation.readTextFile(shaderFileInputStream)
        GLES20.glShaderSource(shader, shaderString)
        GLES20.glCompileShader(shader)

        val compiled = IntArray(1)
        GLES20.glGetShaderiv(shader, GLES20.GL_COMPILE_STATUS, compiled, 0)

        if (compiled[0] == 0) {
            Log.e("ShaderError", "Could not compile shader " + ctx.resources.getResourceEntryName(shaderIdRes))
            Log.e("ShaderError", GLES20.glGetShaderInfoLog(shader))
            GLES20.glDeleteShader(shader)
            shader = 0
        }




        return shader
    }

    fun initShaders(ctx: Context) {
        val vertexShader = loadShader(GLES20.GL_VERTEX_SHADER, R.raw.vp, ctx)
        val fragmentShader = loadShader(GLES20.GL_FRAGMENT_SHADER, R.raw.fp, ctx)

        // create empty OpenGL ES Program
        shaderProgram = GLES20.glCreateProgram()

        // add the vertex shader to program
        GLES20.glAttachShader(shaderProgram, vertexShader)

        // add the fragment shader to program
        GLES20.glAttachShader(shaderProgram, fragmentShader)

        // creates OpenGL ES program executables
        GLES20.glLinkProgram(shaderProgram)


    }


}

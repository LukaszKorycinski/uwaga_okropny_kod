package com.lukaszk.dell.gamedevlukasz.geom

/**
 * Created by Lukasz Korycinski on 02.11.2017.
 */

class Vec2 {

    private var x: Float = 0.toFloat()
    private var y: Float = 0.toFloat()

    constructor() {
        x = 0.0f
        y = 0.0f
    }

    constructor(a: Vec2) {
        x = a.x
        y = a.y
    }

    constructor(a: Float, b: Float) {
        x = a
        y = b
    }


    fun set(vec: Vec2) {
        x = vec.x
        y = vec.y
    }


    fun kat(v1: Vec2, v2: Vec2): Float {
        val l1 = v1.x * v1.x + v1.y * v1.y
        val l2 = v2.x * v2.x + v2.y * v2.y


        return (Math.acos((v1.x * v2.x + v1.y * v2.y).toDouble()) / (l1 * l2)).toFloat()
    }


    fun rotate(angle: Float) {
        var angle = angle
        angle = 3.1415f * angle / 180.0f
        x = x * Math.cos(angle.toDouble()).toFloat() - y * Math.sin(angle.toDouble()).toFloat()
        y = x * Math.sin(angle.toDouble()).toFloat() + y * Math.cos(angle.toDouble()).toFloat()
    }


    operator fun set(a: Float, b: Float) {
        x = a
        y = b
    }


    fun normalize() {
        val l = Math.sqrt((x * x + y * y).toDouble()).toFloat()

        x = x / l
        y = y / l
    }


    fun setLength(l: Float) {
        normalize()

        x = x * l
        y = y * l
    }


}




package com.lukaszk.dell.gamedevlukasz.geom

class VertexVNT {
    var v3f = Vector3f()
    var n3f = Vector3f()
    var t2f = Vector2f()

    constructor() {}

    constructor(v3f: Vector3f, n3f: Vector3f, t2f: Vector2f) {
        this.v3f = v3f
        this.n3f = n3f
        this.t2f = t2f
    }


    override fun equals(obj: Any?): Boolean {
        return obj is VertexVNT && obj.v3f.equals(v3f)!! && obj.n3f.equals(n3f)!! && obj.t2f.equals(t2f)!!
    }


}

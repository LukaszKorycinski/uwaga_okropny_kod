package com.lukaszk.dell.gamedevlukasz.geom.Collada

import com.google.gson.annotations.SerializedName

class Skin {

    @SerializedName("vertex_weights")
    var vertex_weights: Vertex_weights? = null
}

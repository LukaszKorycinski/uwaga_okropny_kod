package com.lukaszk.dell.gamedevlukasz.lukasz_file

import com.lukaszk.dell.gamedevlukasz.geom.Collada.Vector3i
import com.lukaszk.dell.gamedevlukasz.geom.Vector2f
import com.lukaszk.dell.gamedevlukasz.geom.Vector3f

import java.util.ArrayList

open class MeshLukasz {


    var pos = ArrayList<Vector3f>()
    var norm = ArrayList<Vector3f>()
    var texCoord = ArrayList<Vector2f>()
    var indices = ArrayList<Short>()


    fun compare(i: Int, j: Int): Boolean {
        if ( !pos[i].equals(pos[j]) )
            return false

        if ( !norm[i].equals(norm[j]))
            return false

        return if (!texCoord[i].equals(texCoord[j])) false else true

    }

}

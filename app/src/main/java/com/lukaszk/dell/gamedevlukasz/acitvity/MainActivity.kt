package com.lukaszk.dell.gamedevlukasz.acitvity

import android.app.Activity
import android.os.Bundle
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.content.ContextCompat
import com.lukaszk.dell.gamedevlukasz.R
import com.lukaszk.dell.gamedevlukasz.gl.MyGLSurfaceView
import android.widget.RelativeLayout
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import kotlinx.android.synthetic.main.activity_main.*
import android.view.LayoutInflater
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T




class MainActivity : Activity() {

    private lateinit var mGLView: MyGLSurfaceView

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mGLView = MyGLSurfaceView(this)

        setContentView(mGLView)

        val inflater = layoutInflater
        addContentView(inflater.inflate(R.layout.activity_main, null), ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
    }

    override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {
        val x = Math.round(ev!!.getX())
        val y = Math.round(ev.getY())

        if (pressed(upButton, x, y)) {
            mGLView.upOnClick()
            return true
        }

        if (pressed(downButton, x, y)) {
            mGLView.downOnClick()
            return true
        }

        return super.dispatchTouchEvent(ev)
    }

    private fun pressed(view: View, x: Int, y: Int): Boolean {
        if (x > view.left && x < view.right && y > view.top && y < view.bottom) {
            return true
        }
        return false
    }
}

package com.lukaszk.dell.gamedevlukasz.gl

/**
 * Created by dell on 07.06.2017.
 */

import java.nio.FloatBuffer
import java.nio.ShortBuffer

import android.opengl.GLES20

import com.lukaszk.dell.gamedevlukasz.util.DrawHelper

class SubMesh {

    private var indexCount: Int = 0

    // Buffers
    private var vb: FloatBuffer? = null
    private var nb: FloatBuffer? = null
    private var ib: ShortBuffer? = null

    fun setVertexBuffer(vb: FloatBuffer) {
        this.vb = vb
    }

    fun setIndexBuffer(ib: ShortBuffer) {
        this.ib = ib
    }

    fun setNormalBuffer(nb: FloatBuffer) {
        this.nb = nb
    }

    fun setIndexCount(count: Int) {
        indexCount = count
    }

    fun draw(dh: DrawHelper) {
        // Send in vertex positions
        vb!!.position(0)
        GLES20.glVertexAttribPointer(dh.vertexHandle, 3, GLES20.GL_FLOAT, false, 0, vb)

        // Send in normals
        nb!!.position(0)
        GLES20.glVertexAttribPointer(dh.normalHandle, 3, GLES20.GL_FLOAT, false, 0, nb)

        ib!!.position(0)
        GLES20.glDrawElements(GLES20.GL_TRIANGLES, indexCount, GLES20.GL_UNSIGNED_SHORT, ib)
    }

}

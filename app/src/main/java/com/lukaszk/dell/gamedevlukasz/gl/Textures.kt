package com.lukaszk.dell.gamedevlukasz.gl

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.opengl.GLES20
import android.opengl.GLUtils
import android.util.Log

import com.lukaszk.dell.gamedevlukasz.R

/**
 * Created by dell on 06.07.2017.
 */

object Textures {
    var textures = IntArray(1)
    internal var ILE_TEKSTUR = 1

    fun loadTexture(context: Context, resourceId: Int) {


        GLES20.glGenTextures(ILE_TEKSTUR, textures, 0)

        if (textures[0] != 0) {
            val options = BitmapFactory.Options()
            options.inScaled = false   // No pre-scaling

            // Read in the resource
            val bitmap = BitmapFactory.decodeResource(context.resources, resourceId, options)

            // Bind to the texture in OpenGL
            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textures[0])

            // Set filtering
            GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_NEAREST)
            GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_NEAREST)

            // Load the bitmap into the bound texture.
            GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, bitmap, 0)

            // Recycle the bitmap, since its data has been loaded into OpenGL.
            bitmap.recycle()
        }

        if (textures[0] == 0) {
            throw RuntimeException("Error loading texture. " + context.resources.getResourceName(resourceId))
        }
    }

    fun bindTexture(i: Int, shaderProgram: Int) {
        val textureHandler = GLES20.glGetUniformLocation(shaderProgram, "tex0")
        GLES20.glActiveTexture(GLES20.GL_TEXTURE0)
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textures[i])
        GLES20.glUniform1i(textureHandler, 0)
    }

    fun loadAll(ctx: Context) {
        loadTexture(ctx, R.drawable.tex)
        Log.e("textures", "R.drawable.tex")
    }


}

package com.lukaszk.dell.gamedevlukasz.gl

import android.content.Context
import android.opengl.GLSurfaceView

import com.lukaszk.dell.gamedevlukasz.gl.MyGLRenderer

/**
 * Created by dell on 07.06.2017.
 */

class MyGLSurfaceView(context: Context) : GLSurfaceView(context) {

    private val mRenderer: MyGLRenderer

    fun upOnClick() {
        mRenderer.upOnClick()
    }

    fun downOnClick() {
        mRenderer.downOnClick()
    }

    init {

        // Create an OpenGL ES 2.0 context
        setEGLContextClientVersion(2)

        mRenderer = MyGLRenderer(context)

        // Set the Renderer for drawing on the GLSurfaceView
        setRenderer(mRenderer)
    }
}

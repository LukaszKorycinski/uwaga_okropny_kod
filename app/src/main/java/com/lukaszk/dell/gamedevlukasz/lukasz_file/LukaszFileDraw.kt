package com.lukaszk.dell.gamedevlukasz.lukasz_file

import android.opengl.GLES20
import android.util.Log

import com.lukaszk.dell.gamedevlukasz.geom.AnimMeshLukasz
import com.lukaszk.dell.gamedevlukasz.gl.MatrixMy
import com.lukaszk.dell.gamedevlukasz.gl.Shader
import com.lukaszk.dell.gamedevlukasz.gl.Textures

import java.nio.ByteBuffer
import java.nio.ByteOrder
import java.nio.FloatBuffer
import java.nio.ShortBuffer

class LukaszFileDraw(mesh: AnimMeshLukasz) {


    private val vertexBuffer: FloatBuffer
    private val normalBuffer: FloatBuffer
    private val texcoordBuffer: FloatBuffer
    private val drawListBuffer: ShortBuffer
    private val bonesMatricesArray: FloatArray
    private val posesQty: Int
    private val bonesQty: Int
    private val indicesCout: Int

    internal var testFrame = 0

    init {
        Log.e("pos", "" + mesh.pos.size)//37
        Log.e("nor", "" + mesh.norm.size)//37
        Log.e("tex", "" + mesh.texCoord.size)//37
        Log.e("ind", "" + mesh.indices.size)//72
        Log.e("bInd", "" + mesh.boneIndex.size)//37
        Log.e("bMat", "" + mesh.bonesMatrices.size)//6
        Log.e("bonesQty", "" + mesh.bonesQty!!)//2
        Log.e("posesQty", "" + mesh.posesQty!!)//3


        val vertexArray = FloatArray(mesh.pos.size * 4)
        val normalArray = FloatArray(mesh.pos.size * 3)
        val texcoordArray = FloatArray(mesh.pos.size * 2)
        val indicesShort = ShortArray(mesh.indices.size)
        bonesMatricesArray = FloatArray(mesh.bonesQty!! * mesh.posesQty!! * 16)

        bonesQty = mesh.bonesQty!!
        posesQty = mesh.posesQty!!

        var vLiterator = 0
        var nLiterator = 0
        var tLiterator = 0




        for (i in mesh.pos.indices) {
            vertexArray[vLiterator] = mesh.pos[i].x
            vLiterator++
            vertexArray[vLiterator] = mesh.pos[i].y
            vLiterator++
            vertexArray[vLiterator] = mesh.pos[i].z
            vLiterator++
            vertexArray[vLiterator] = mesh.boneIndex[i].toFloat()
            vLiterator++

            normalArray[nLiterator] = mesh.norm[i].x
            nLiterator++
            normalArray[nLiterator] = mesh.norm[i].y
            nLiterator++
            normalArray[nLiterator] = mesh.norm[i].z
            nLiterator++

            texcoordArray[tLiterator] = mesh.texCoord[i].x
            tLiterator++
            texcoordArray[tLiterator] = mesh.texCoord[i].y
            tLiterator++
        }



        for (i in mesh.indices.indices) {
            indicesShort[i] = mesh.indices[i]
        }




        for (i in mesh.bonesMatrices.indices) {
            var mLiterator = 0
            bonesMatricesArray[i * 16 + mLiterator] = mesh.bonesMatrices[i].matrix[mLiterator]
            mLiterator++
            bonesMatricesArray[i * 16 + mLiterator] = mesh.bonesMatrices[i].matrix[mLiterator]
            mLiterator++
            bonesMatricesArray[i * 16 + mLiterator] = mesh.bonesMatrices[i].matrix[mLiterator]
            mLiterator++
            bonesMatricesArray[i * 16 + mLiterator] = mesh.bonesMatrices[i].matrix[mLiterator]
            mLiterator++
            bonesMatricesArray[i * 16 + mLiterator] = mesh.bonesMatrices[i].matrix[mLiterator]
            mLiterator++
            bonesMatricesArray[i * 16 + mLiterator] = mesh.bonesMatrices[i].matrix[mLiterator]
            mLiterator++
            bonesMatricesArray[i * 16 + mLiterator] = mesh.bonesMatrices[i].matrix[mLiterator]
            mLiterator++
            bonesMatricesArray[i * 16 + mLiterator] = mesh.bonesMatrices[i].matrix[mLiterator]
            mLiterator++
            bonesMatricesArray[i * 16 + mLiterator] = mesh.bonesMatrices[i].matrix[mLiterator]
            mLiterator++
            bonesMatricesArray[i * 16 + mLiterator] = mesh.bonesMatrices[i].matrix[mLiterator]
            mLiterator++
            bonesMatricesArray[i * 16 + mLiterator] = mesh.bonesMatrices[i].matrix[mLiterator]
            mLiterator++
            bonesMatricesArray[i * 16 + mLiterator] = mesh.bonesMatrices[i].matrix[mLiterator]
            mLiterator++
            bonesMatricesArray[i * 16 + mLiterator] = mesh.bonesMatrices[i].matrix[mLiterator]
            mLiterator++
            bonesMatricesArray[i * 16 + mLiterator] = mesh.bonesMatrices[i].matrix[mLiterator]
            mLiterator++
            bonesMatricesArray[i * 16 + mLiterator] = mesh.bonesMatrices[i].matrix[mLiterator]
            mLiterator++
            bonesMatricesArray[i * 16 + mLiterator] = mesh.bonesMatrices[i].matrix[mLiterator]
        }


        //        for (int i = 0; i < colladaOpenGlAdapter.getBones().size(); i++){
        //            bonesPosesArray[i]=colladaOpenGlAdapter.getBones().get(i);
        //        }


        indicesCout = mesh.indices.size


        var bb = ByteBuffer.allocateDirect(mesh.pos.size * 4 * 4)
        bb.order(ByteOrder.nativeOrder())
        vertexBuffer = bb.asFloatBuffer()

        bb = ByteBuffer.allocateDirect(mesh.pos.size * 3 * 4)
        bb.order(ByteOrder.nativeOrder())
        normalBuffer = bb.asFloatBuffer()

        bb = ByteBuffer.allocateDirect(mesh.pos.size * 2 * 4)
        bb.order(ByteOrder.nativeOrder())
        texcoordBuffer = bb.asFloatBuffer()

        bb = ByteBuffer.allocateDirect(indicesCout * 2)
        bb.order(ByteOrder.nativeOrder())
        drawListBuffer = bb.asShortBuffer()

        vertexBuffer.put(vertexArray)
        vertexBuffer.position(0)

        normalBuffer.put(normalArray)
        normalBuffer.position(0)

        texcoordBuffer.put(texcoordArray)
        texcoordBuffer.position(0)

        drawListBuffer.put(indicesShort)
        drawListBuffer.position(0)

    }


    fun draw() {
        val currentBonesPosesArray = interpolateSkeletons(0.0f)

        GLES20.glUseProgram(Shader.shaderProgram)

        Textures.bindTexture(0, Shader.shaderProgram)
        val mMVPMatrixHandle = GLES20.glGetUniformLocation(Shader.shaderProgram, "uMVPMatrix")
        GLES20.glUniformMatrix4fv(mMVPMatrixHandle, 1, false, MatrixMy.mViewProjectionMatrix, 0)

        val mPositionHandle = GLES20.glGetAttribLocation(Shader.shaderProgram, "vPosition")
        val mNormalHandle = GLES20.glGetAttribLocation(Shader.shaderProgram, "vNormal")
        val mTexCoordHandle = GLES20.glGetAttribLocation(Shader.shaderProgram, "vTexCoord")

        val bonesMatricesHandle = GLES20.glGetUniformLocation(Shader.shaderProgram, "bonesMatrices")

        GLES20.glUniformMatrix4fv(bonesMatricesHandle, bonesQty, false, currentBonesPosesArray, 0)

        GLES20.glEnableVertexAttribArray(mPositionHandle)
        GLES20.glVertexAttribPointer(mPositionHandle, 4, GLES20.GL_FLOAT, false, 0, vertexBuffer)
        GLES20.glEnableVertexAttribArray(mNormalHandle)
        GLES20.glVertexAttribPointer(mNormalHandle, 3, GLES20.GL_FLOAT, false, 0, normalBuffer)
        GLES20.glEnableVertexAttribArray(mTexCoordHandle)
        GLES20.glVertexAttribPointer(mTexCoordHandle, 2, GLES20.GL_FLOAT, false, 0, texcoordBuffer)

        GLES20.glDrawElements(GLES20.GL_TRIANGLES, indicesCout, GLES20.GL_UNSIGNED_SHORT, drawListBuffer)

        GLES20.glDisableVertexAttribArray(mPositionHandle)
        GLES20.glDisableVertexAttribArray(mNormalHandle)
        GLES20.glDisableVertexAttribArray(mTexCoordHandle)
    }


    private fun interpolateSkeletons(v: Float): FloatArray {

        val currMatrix = FloatArray(16 * bonesQty)//*bonesQty


        //        for(int i=0;i<16;i++) {
        //            currMatrix[i] = bonesMatricesArray[i+testFrame*16];
        //            Log.e("frame", ""+testFrame);
        //        }


        for (i in 0 until bonesQty) {

            for (j in 0..15) {
                currMatrix[i * 16 + j] = bonesMatricesArray[j + 16 * i +/*i*16*posesQty +*/ 16 * testFrame]


                //Log.e("ind", "" + (j + 16 * testFrame))

            }

        }


        //pose1bone1 pose2bone1 pose3bone1 pose1bone2 pose2bone2 pose3bone2    pose 3   bone 2


        testFrame++
        if (testFrame >= posesQty)
            testFrame = 0

        return currMatrix//TODO to nie jest skończone xd
    }


}

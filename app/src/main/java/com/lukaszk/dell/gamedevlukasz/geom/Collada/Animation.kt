package com.lukaszk.dell.gamedevlukasz.geom.Collada

import com.google.gson.annotations.SerializedName

class Animation {

    @SerializedName("source")
    var source: List<Source>? = null
}

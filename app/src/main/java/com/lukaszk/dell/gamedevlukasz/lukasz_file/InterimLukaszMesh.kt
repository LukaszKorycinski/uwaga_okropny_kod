package com.lukaszk.dell.gamedevlukasz.lukasz_file

import com.lukaszk.dell.gamedevlukasz.geom.AnimMeshLukasz
import com.lukaszk.dell.gamedevlukasz.geom.VertexVNT

import java.util.ArrayList

class InterimLukaszMesh {

    var animAndIndexedGeom = AnimMeshLukasz()

    var nonIndexedGeom: ArrayList<VertexVNT> = ArrayList()

    var bonesNames: ArrayList<String> = ArrayList()


}

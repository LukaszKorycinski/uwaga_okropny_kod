package com.lukaszk.dell.gamedevlukasz.gl

import android.content.Context
import android.opengl.GLES20
import android.opengl.GLSurfaceView
import com.lukaszk.dell.gamedevlukasz.lukasz_file.LoadFromLukasz
import com.lukaszk.dell.gamedevlukasz.lukasz_file.LukaszFileDraw
import javax.microedition.khronos.egl.EGLConfig
import javax.microedition.khronos.opengles.GL10
import android.util.Log
import com.lukaszk.dell.gamedevlukasz.geom.DrawColladaModel
import com.lukaszk.dell.gamedevlukasz.geom.LoadFromCollada

class MyGLRenderer
internal constructor(private val ctx: Context) : GLSurfaceView.Renderer {
    private lateinit var drawColladaModel: DrawColladaModel
    //private lateinit var lukaszFileDraw: LukaszFileDraw

    override fun onSurfaceCreated(unused: GL10, config: EGLConfig) {

        GLES20.glClearColor(0.8f, 0.4f, 0.8f, 1.0f)
        GLES20.glDisable(GLES20.GL_CULL_FACE)
        //GLES20.glEnable(GLES20.GL_DEPTH_TEST)


        Textures.loadAll(ctx)



        drawColladaModel = DrawColladaModel(LoadFromCollada(ctx).load())

        //val animMeshLukasz = LoadFromLukasz(ctx).load()
        //lukaszFileDraw = LukaszFileDraw(animMeshLukasz)

        Shader.initShaders(ctx)
    }


    override fun onDrawFrame(unused: GL10) {
        // Redraw background color
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT)

        MatrixMy.camMatrix()

        //lukaszFileDraw.draw()
        drawColladaModel.draw();
    }

    fun upOnClick() {
        MatrixMy.posZ += 0.1f
    }

    fun downOnClick() {
        MatrixMy.posZ -= 0.1f
    }

    override fun onSurfaceChanged(unused: GL10, width: Int, height: Int) {
        GLES20.glViewport(0, 0, width, height)

        val aspectratio = width.toFloat() / height

        MatrixMy.perspectiveINV(aspectratio)
        //Matrix.perspectiveM(MatrixMy.mProjectionMatrix, 0, 60.0f, ratio, 0.1f, 20.0f);
        //Matrix.frustumM(MatrixMy.mProjectionMatrix, 0, -ratio, ratio, -1, 1, 0.1f, 7);

    }

}

package com.lukaszk.dell.gamedevlukasz.geom.Collada

import com.google.gson.annotations.SerializedName

/**
 * Created by dell on 09.06.2017.
 */

class ColladaModel {

    @SerializedName("COLLADA")
    var collada: Collada? = null
}

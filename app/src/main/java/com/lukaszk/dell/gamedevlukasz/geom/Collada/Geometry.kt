package com.lukaszk.dell.gamedevlukasz.geom.Collada

import com.google.gson.annotations.SerializedName

/**
 * Created by dell on 08.06.2017.
 */

class Geometry {


    @SerializedName("mesh")
    var mesh: Mesh? = null
}

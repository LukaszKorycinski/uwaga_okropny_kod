package com.lukaszk.dell.gamedevlukasz.gl

import android.opengl.Matrix
import android.util.Log

import com.lukaszk.dell.gamedevlukasz.geom.Vec2
import com.lukaszk.dell.gamedevlukasz.geom.Vector3f

/**
 * Created by dell on 04.07.2017.
 */

class MatrixMy {

    companion object {

        var mProjectionMatrix = FloatArray(16)
        var mViewProjectionMatrix = FloatArray(16)
        var ViewMatrix = FloatArray(16)
        //public static float[] ZeroMatrix = new float[16];

        var rotX: Float = 0.0f
        var rotY: Float = 0.0f
        var posX: Float = 0.0f
        var posY: Float = 0.0f
        var posZ: Float = -5.5f

        fun camMatrix() {
            Matrix.setIdentityM(ViewMatrix, 0)

            Matrix.rotateM(ViewMatrix, 0, rotX, 1.0f, 0.0f, 0.0f)
            Matrix.rotateM(ViewMatrix, 0, rotY, 0.0f, 1.0f, 0.0f)
            Matrix.translateM(ViewMatrix, 0, posX, posY, posZ)

            Matrix.setIdentityM(mViewProjectionMatrix, 0)

            Matrix.multiplyMM(mViewProjectionMatrix, 0, mProjectionMatrix, 0, ViewMatrix, 0)
        }


        fun perspectiveINV(aspect: Float) {
            val angle = 1.3f

            val near = 0.1f
            val far = 15.0f

            val f = Math.tan(0.5 * (Math.PI - angle)).toFloat()
            val range = near - far

            mProjectionMatrix[0] = f / aspect
            mProjectionMatrix[1] = 0f
            mProjectionMatrix[2] = 0f
            mProjectionMatrix[3] = 0f

            mProjectionMatrix[4] = 0f
            mProjectionMatrix[5] = f
            mProjectionMatrix[6] = 0f
            mProjectionMatrix[7] = 0f

            mProjectionMatrix[8] = 0f
            mProjectionMatrix[9] = 0f
            mProjectionMatrix[10] = far / range
            mProjectionMatrix[11] = -1f

            mProjectionMatrix[12] = 0f
            mProjectionMatrix[13] = 0f
            mProjectionMatrix[14] = near * far / range
            mProjectionMatrix[15] = 0f
        }






        fun unproject(xkursor: Float, ykursor: Float, szer: Float, wys: Float): Vec2 {
            // Initialize auxiliary variables.
            val worldPos = Vec2()

            // SCREEN height & width (ej: 320 x 480)

            // Auxiliary matrix and vectors
            // to deal with ogl.
            val invertedMatrix: FloatArray
            val transformMatrix: FloatArray
            val normalizedInPoint: FloatArray
            val outPoint: FloatArray
            invertedMatrix = FloatArray(16)
            transformMatrix = FloatArray(16)
            normalizedInPoint = FloatArray(4)
            outPoint = FloatArray(4)

            // Invert y coordinate, as android uses
            // top-left, and ogl bottom-left.
            val oglTouchY = (wys - ykursor).toInt()

            /* Transform the screen point to clip
        space in ogl (-1,1) */
            normalizedInPoint[0] = (xkursor * 2.0f / szer - 1.0).toFloat() * 109
            normalizedInPoint[1] = (oglTouchY * 2.0f / wys - 1.0).toFloat() * 109
            normalizedInPoint[2] = -1.0f
            normalizedInPoint[3] = 1.0f

            /* Obtain the transform matrix and
        then the inverse. */

            Matrix.multiplyMM(
                    transformMatrix, 0,
                    mProjectionMatrix, 0,
                    ViewMatrix, 0)
            Matrix.invertM(invertedMatrix, 0,
                    transformMatrix, 0)

            /* Apply the inverse to the point
        in clip space */
            Matrix.multiplyMV(
                    outPoint, 0,
                    invertedMatrix, 0,
                    normalizedInPoint, 0)

            if (outPoint[3].toDouble() == 0.0) {//error
                return worldPos
            }

            // Divide by the 3rd component to find
            // out the real position.
            worldPos.set(
                    outPoint[0] / outPoint[3],
                    outPoint[1] / outPoint[3])


            //Vec2 wynik=new Vec2();

            return worldPos
        }
    }


}
